ics-ans-role-proxyclients
===================

Ansible role to install proxy client settings.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
proxyclient_url: "http://proxy:8080"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-proxyclient
```

License
-------

BSD 2-clause
